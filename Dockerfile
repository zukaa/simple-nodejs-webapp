FROM docker.io/alpine
RUN apk update && apk add nodejs
COPY ./ /app
ENV APPLICATION_INSTANCE=example
EXPOSE 8080
WORKDIR /app
ENTRYPOINT ["node","count-server.js"]
